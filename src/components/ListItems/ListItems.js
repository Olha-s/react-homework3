import React from 'react';
import CardItem from "../CardItem/CardItem";
import PropTypes from 'prop-types';
import './ListItems.scss'
import {connect} from "react-redux";

const ListItems = ({items, onClick, page, addToFavorites, deleteFavorites}) => {

          const cards = items.map(el => <CardItem
                                          card={el}
                                          key={el.article}
                                          onClick={onClick}
                                          addToFavorites={addToFavorites}
                                          deleteFavorites={deleteFavorites}
                                          page={page}/>);
        return (
            <div className="ListItems">
                {cards}
            </div>
        );
    };



ListItems.propTypes = {
    items: PropTypes.array.isRequired,
    onClick: PropTypes.func.isRequired,
    addToFavorites: PropTypes.func.isRequired,
    deleteFavorites: PropTypes.func.isRequired
};

const mapStoreToProps = ({items}) => {
    return{
        items
     }
};

export default connect(mapStoreToProps)(ListItems);
