import React, {useEffect, useState} from 'react';
import {star} from './star';
import PropTypes from "prop-types";
import {connect} from "react-redux";

const FavoritesIcon = ({article, addToFavorites, deleteFavorites, page, cardsFavorites}) => {

    const [starShowFilled, setFilled] = useState(false);

    useEffect(() => {
        if (cardsFavorites.find(card => card.article === article)) {
            setFilled(true)
        }
    }, [cardsFavorites, article]);

    const starFilled = () => {
        if (!starShowFilled) {
            addToFavorites(article);
            setFilled(true);
        } else {
            deleteFavorites(article);
            setFilled(false);
        }
    };

    const starDelete = () => {
        deleteFavorites(article);
    }
    return (
        <>
            <div onClick={starFilled}>
                {page === "/main" && star(starShowFilled)}
                {page === "/" && star(starShowFilled)}
            </div>
            <div onClick={starDelete}>
                {page === "/favorites" && star(true)}

            </div>
        </>
    );
};

FavoritesIcon.propTypes = {
    article: PropTypes.string.isRequired,
    addToFavorites: PropTypes.func,
    deleteFavorites: PropTypes.func,
    cardsFavorites: PropTypes.array.isRequired,
    page: PropTypes.string.isRequired
};
const mapStoreToProps = ({cardsFavorites}) => {
    return {
        cardsFavorites
    }
};
export default connect(mapStoreToProps)(FavoritesIcon);



