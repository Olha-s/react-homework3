import {combineReducers} from "redux";
import initialStore from "./initialStore";
import {
    ADD_CART,
    ADD_FAVORITES,
    DELETE_FROM_CART,
    DELETE_FROM_FAVORITES,
    LOAD_CARDS,
    MODAL_CART_SHOW,
    MODAL_SHOW
} from "./actions/actions";

const cardsReducer = (cardsFromStore = initialStore.items, action) => {
    switch (action.type) {
        case LOAD_CARDS:
            return action.payload;

        default:
            return cardsFromStore;
    }
};
const modalShowReducer = (modalShowFromStore = initialStore.modalShow, action) => {
    switch (action.type) {
        case MODAL_SHOW:
            return action.payload;
        default:
            return modalShowFromStore;
    }
}

const modalShowCartReducer = (modalShowCartFromStore = initialStore.modalShowCart, action) => {
    switch (action.type) {
        case MODAL_CART_SHOW:
            return action.payload;
        default:
            return modalShowCartFromStore;
    }
};

const cardsCartReducer = (cardsCartFromStore = initialStore.cardsCart, action) => {
    switch (action.type) {
        case ADD_CART:
            return [...cardsCartFromStore, action.payload];
        case DELETE_FROM_CART:
            return [...cardsCartFromStore.filter(card => card.article !== action.payload)];
        default:
            return cardsCartFromStore;
    }
};

const cardsFavoritesReducer = (cardsFavoritesFromStore = initialStore.cardsFavorites, action) => {
    switch (action.type) {
        case ADD_FAVORITES:
            return [...cardsFavoritesFromStore, action.payload];
        case DELETE_FROM_FAVORITES:
            return [...cardsFavoritesFromStore.filter(card => card.article !== action.payload)];

        default:
            return cardsFavoritesFromStore;
    }
};

export default combineReducers({
    items: cardsReducer,
    modalShow: modalShowReducer,
    modalShowCart: modalShowCartReducer,
    cardsCart: cardsCartReducer,
    cardsFavorites: cardsFavoritesReducer

});
