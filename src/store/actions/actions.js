export const LOAD_CARDS = "LOAD_CARDS";
export const MODAL_SHOW = "MODAL_SHOW";
export const MODAL_CART_SHOW = "MODAL_CART_SHOW ";
export const ADD_CART = "ADD_CART";
export const DELETE_FROM_CART = "DELETE_FROM_CART";
export const ADD_FAVORITES = "ADD_FAVORITES";
export const DELETE_FROM_FAVORITES = "DELETE_FROM_FAVORITES";

export const loadCardsAction = () => (dispatch) => {
    return (

        fetch('./items.json').then(r => r.json())
            .then(data => {
                dispatch({type: 'LOAD_CARDS', payload: data})
            })
    )
};

export const modalShowAction = (isOpen) => (dispatch) => {
    return (
        dispatch({type: 'MODAL_SHOW', payload: isOpen})
    )

};

export const modalShowCartAction = (isOpen) => (dispatch) => {
    return (
        dispatch({type: 'MODAL_CART_SHOW ', payload: isOpen})
    )
};

export const addCardsCartAction = (cardAdd) => (dispatch) => {
    return (
        dispatch({type: 'ADD_CART', payload: cardAdd})
         )
};

export const deleteCardsCartAction = (id) => (dispatch) => {
    return (
            dispatch({type: 'DELETE_FROM_CART', payload: id})
    )
};

export const addCardsFavoritesAction = (cardAdd) => (dispatch) => {
    return (
        dispatch({type: 'ADD_FAVORITES', payload: cardAdd})
    )
};

export const deleteCardsFavoritesAction = (id) => (dispatch) => {
    return(
        dispatch({type: 'DELETE_FROM_FAVORITES', payload: id})
    )
};