const initialStore = {
    items: [],
    modalShow: false,
    modalShowCart: false,
    cardsCart: [],
    cardsFavorites: []
};
export default initialStore;