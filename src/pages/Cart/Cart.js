import React from 'react';
import CardItem from "../../components/CardItem/CardItem";
import PropTypes from "prop-types";
import {connect} from "react-redux";

const Cart = ({cardsCart, page, deleteCard}) => {

    const cards = cardsCart.map(el => <CardItem card={el} key={el.article} page={page} deleteCard={deleteCard}/>);
    return (
        <div className="ListItems">
            {cards}
        </div>
    );
};

Cart.propTypes = {
    deleteCard: PropTypes.func.isRequired,
    cardsCart: PropTypes.array.isRequired,
    page: PropTypes.string.isRequired
};

const mapStoreToProps = ({cardsCart}) => {
    return {
        cardsCart
    }
};
export default connect(mapStoreToProps)(Cart);